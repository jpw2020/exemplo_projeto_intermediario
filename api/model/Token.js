const mongoose = require('../data')

var tokenSchema = new mongoose.Schema({
    usuario: {
        type: String,
        required: true
    },
    token: {
        type: String,
        required: true,
        unique: true
    }
})

var Token = new mongoose.model("Token", tokenSchema)

module.exports = Token
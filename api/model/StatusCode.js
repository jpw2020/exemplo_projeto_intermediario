const mongoose = require('../data')

var statusCodeSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true
    },
    code: {
        type: Number,
        required: true
    },
    image_url: {
        type: String
    }
})

var StatusCode = new mongoose.model("StatusCodes", statusCodeSchema)

module.exports = StatusCode
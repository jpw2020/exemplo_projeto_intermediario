const express = require('express')
const router = express.Router()
var StatusCode = require('../model/StatusCode')

router.get('/', function(req, res){
    var limit = parseInt(req.query.limit)
    var filter = {}
    if(req.query.code){
        var filter = { code: req.query.code }
    }
    var result = StatusCode.find(filter,function(err, doc){
        if(!err){
            res.status(200).json(doc)
        } else{
            res.status(500).json({erro: "Erro ao acessar recurso", mongoose: err.message})
        }
    }).limit(limit)
})

router.post('/', function(req, res){
    var status_code = req.body
    var novo_status = new StatusCode(status_code)
    novo_status.save(function(err){
        if (!err){
            res.json(novo_status)
        } else {
            res.status(500).json({erro: "Erro ao salvar"})
        }
    })
})

router.put("/:id", function(req, res){
    var id = req.params.id
    var status_code = req.body
    StatusCode.findByIdAndUpdate(id, status_code, function(err, doc){
        if (!err){
            res.json(doc)
        } else {
            res.status(500).json({erro: "Erro ao salvar"})
        }
    })
})

router.delete("/:id", function(req, res){
    var id = req.params.id
    StatusCode.findByIdAndDelete(id, function(err, doc){
        if (!err){
            res.json(doc)
        } else {
            res.status(500).json({erro: "Erro ao deletar"})
        }
    })
})

module.exports = router
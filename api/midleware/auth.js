const express = require('express')
const route = express.Router()
const Token = require('../model/Token')

route.use(function(req, res, next){

    if(!req.headers.authorization){
        res.status(400).json({erro: "Faltando o token"})
    }

    var token = req.headers.authorization.replace('Bearer ', '')  

    Token.findOne({token : token}, function(err, doc){
        console.log(doc)
        if(err){
            res.status(500).json({erro: "Erro ao pesquisar token"})
        }

        if(!doc){
            res.status(401).json({erro: "token não autorizado"})
        }
        
        next()
    })
})

module.exports = route
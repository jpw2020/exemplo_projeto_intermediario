const mongoose = require('mongoose')

const url = `mongodb+srv://${process.env.MONGO_USER}:${process.env.MONGO_PASSWORD}@cluster0.6vpr4.mongodb.net/${process.env.MONGO_DB}?retryWrites=true&w=majority`
mongoose.connect(url, {
    useNewUrlParser: true,
    useUnifiedTopology: true
})

module.exports = mongoose
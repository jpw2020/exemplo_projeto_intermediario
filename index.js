const express = require('express')
const app = express()
const cors = require('cors')
const routes = {
    "status_code": require('./api/routes/status_code'),
    "auth": require("./api/midleware/auth")
}

app.listen(process.env.PORT || 3000)
app.use(routes.auth)
app.use(express.json())
app.use(cors())
app.use("/status_code", routes.status_code)

app.get("/", function(req, res){
    res.json({"version": 0.1})
})